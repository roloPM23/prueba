
package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import models.Producto;
import views.MainFrame;

public class ProductoController {
    private MainFrame frame;
    private JFileChooser fc;
    private Producto producto;

    public ProductoController(MainFrame frame) {
        this.frame = frame;
        fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("txt files (*.txt)", "txt"));
    }    
    
    public void eventosBotones(String actionCommand) {
        switch(actionCommand) {
            case "Seleccionar":
                fc.showOpenDialog(frame);  
                producto = seleccionarProducto(fc.getSelectedFile());
                frame.setDatosProducto(producto);
                break;
            case "Limpiar":
                frame.Limpiar();
                break;
            case "Guardar":
                fc.showSaveDialog(frame);
                producto = frame.getDatosProducto();
                guardarProducto(fc.getSelectedFile());
                break;
                
        }
    }
    
    public void guardarProducto(File file) {
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file + ".txt")); 
            w.writeObject(producto);
            w.flush();
        } catch (IOException ex) {
        }
    }
    
    private Producto seleccionarProducto(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Producto)ois.readObject();
        } 
        catch(FileNotFoundException e){
            
        } catch (IOException | ClassNotFoundException ex) {
        }
         return null;
    }
}
