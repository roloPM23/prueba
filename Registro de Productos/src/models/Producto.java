
package models;

import java.io.Serializable;

public class Producto implements Serializable{
    public static final int ID[] = {1,2,3,4};
    private int idProducto;
    private String nombreProducto;
    private Double precioProducto;

    public Producto() {
        
    }
    public Producto(int idProducto, String nombre, Double precio) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.precioProducto = precioProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }
    
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public void setPrecioProducto(Double precio) {
        this.precioProducto = precio;
    }

    public int getIdProducto() {
        return idProducto;
    }
    
    public String getNombreProducto() {
        return nombreProducto;
    }

    public Double getPrecioProducto() {
        return precioProducto;
    }
    
    
    
}
